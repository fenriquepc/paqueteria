﻿using System.Collections.Generic;

namespace Paqueteria.Data.Repositories
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        void Insert(T entity);
    }
}