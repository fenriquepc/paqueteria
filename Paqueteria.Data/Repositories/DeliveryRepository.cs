﻿using Paqueteria.Data.Models;
using System.Linq;

namespace Paqueteria.Data.Repositories
{
    public class DeliveryRepository : Repository<Delivery>, IDeliveryRepository
    {
        public Delivery GetById(int id)
        {
            var vehicle = _data.FirstOrDefault(v => v.Id == id);
            return vehicle;
        }

        public override void Insert(Delivery delivery)
        {
            delivery.Id = _data.Count + 1;
            base.Insert(delivery);
        }

        public void Update(Delivery delivery)
        {
            var old = _data.First(d => d.Id == delivery.Id);
            var index = _data.IndexOf(old);
            _data[index] = delivery;
        }
    }
}
