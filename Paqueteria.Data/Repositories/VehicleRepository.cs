﻿using Paqueteria.Data.Models;
using System.Linq;

namespace Paqueteria.Data.Repositories
{
    public class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository()
        {
            _data.Clear();

            _data.Add(
                new Vehicle { 
                    Id = 1 ,
                    Type = VehicleTypeEnum.Truck,
                    Model = "MAN TGX",
                    VehicleRegistration = "0453CMU"
                }
            );;

            _data.Add(
                new Vehicle { 
                    Id = 2,
                    Type = VehicleTypeEnum.Van,
                    Model = "Mercedes Vito",
                    VehicleRegistration = "8204CKJ"
                }
            );
        }

        public Vehicle GetById(int id)
        {
            var vehicle = _data.FirstOrDefault(v => v.Id == id);
            return vehicle;
        }
    }
}
