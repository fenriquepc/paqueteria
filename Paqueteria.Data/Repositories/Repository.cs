﻿using System.Collections.Generic;

namespace Paqueteria.Data.Repositories
{
    public class Repository<T> : IRepository<T>
    {
        protected static readonly IList<T> _data = new List<T>();

        public IEnumerable<T> GetAll()
        {
            return _data;
        }

        public virtual void Insert(T entity)
        {
            _data.Add(entity);
        }
    }
}
