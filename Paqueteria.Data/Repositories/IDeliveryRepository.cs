﻿using Paqueteria.Data.Models;

namespace Paqueteria.Data.Repositories
{
    public interface IDeliveryRepository : IRepository<Delivery>
    {
        Delivery GetById(int id);
        void Update(Delivery delivery);
    }
}