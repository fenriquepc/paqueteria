﻿using Paqueteria.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Paqueteria.Data.Repositories
{
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
        public IEnumerable<Location> GetByVehicle(int vehicleId)
        {
            var result = _data.Where(l => l.VehicleId == vehicleId);
            return result.ToList();
        }
    }
}
