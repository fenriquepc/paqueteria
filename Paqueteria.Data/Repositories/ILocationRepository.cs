﻿using Paqueteria.Data.Models;
using System.Collections.Generic;

namespace Paqueteria.Data.Repositories
{
    public interface ILocationRepository : IRepository<Location>
    {
        IEnumerable<Location> GetByVehicle(int vehicleId);
    }
}