﻿using Paqueteria.Data.Models;

namespace Paqueteria.Data.Repositories
{
    /// <summary>
    /// Interface class for the Vehicle model repository
    /// </summary>
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        /// <summary>
        /// Find a Vehicle by Id 
        /// </summary>
        /// <param name="id">Id of the vehicle to find</param>
        /// <returns>Found vehicle</returns>
        Vehicle GetById(int id);
    }
}
