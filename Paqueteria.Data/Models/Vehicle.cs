﻿namespace Paqueteria.Data.Models
{
    public class Vehicle
    {
        public int Id { get; set; }

        public string VehicleRegistration { get; set; }

        public string Model { get; set; }

        public VehicleTypeEnum Type { get; set; }
    }
}
