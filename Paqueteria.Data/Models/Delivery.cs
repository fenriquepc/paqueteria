﻿using System;

namespace Paqueteria.Data.Models
{
    public class Delivery
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public int? VehicleId { get; set; }
        public bool Delivered { get; set; }
        public DateTime? DeliveredDate { get; set; }
    }
}
