﻿namespace Paqueteria.Data.Models
{
    public enum VehicleTypeEnum
    {
        Motorcycle,
        Van,
        Truck
    }
}
