﻿using System;

namespace Paqueteria.Data.Models
{
    public class Location
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime Date { get; set; }
    }
}
