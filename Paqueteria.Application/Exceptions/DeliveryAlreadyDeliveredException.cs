﻿using System;

namespace Paqueteria.Application.Exceptions
{
    public class DeliveryAlreadyDeliveredException : Exception
    {
        #region Constructors

        public DeliveryAlreadyDeliveredException() { }

        public DeliveryAlreadyDeliveredException(string message) : base(message) { }

        public DeliveryAlreadyDeliveredException(string message, Exception inner) : base(message, inner) { }

        #endregion
    }
}
