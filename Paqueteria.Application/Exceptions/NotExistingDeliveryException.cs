﻿using System;

namespace Paqueteria.Application.Exceptions
{
    public class NotExistingDeliveryException : Exception
    {
        #region Constructors

        public NotExistingDeliveryException() { }

        public NotExistingDeliveryException(string message) : base(message) { }

        public NotExistingDeliveryException(string message, Exception inner) : base(message, inner) { }

        #endregion
    }
}
