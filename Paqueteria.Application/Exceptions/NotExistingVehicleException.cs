﻿using System;

namespace Paqueteria.Application.Exceptions
{
    public class NotExistingVehicleException : Exception 
    {
        #region Constructors

        public NotExistingVehicleException() { }

        public NotExistingVehicleException(string message) : base(message) { }

        public NotExistingVehicleException(string message, Exception inner) : base(message, inner) { }

        #endregion
    }
}
