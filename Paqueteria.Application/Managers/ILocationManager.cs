﻿using Paqueteria.Application.DTO;

namespace Paqueteria.Application.Managers
{
    /// <summary>
    /// Interface for the LocationManager class
    /// </summary>
    public interface ILocationManager
    {
        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="locationRepository">Interface of LocationRepository</param>
        void SaveCurrentVehicleLocation(LocationDTO location);
    }
}