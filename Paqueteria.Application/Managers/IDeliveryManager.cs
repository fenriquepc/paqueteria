﻿using Paqueteria.Application.DTO;

namespace Paqueteria.Application.Managers
{
    /// <summary>
    /// Interface for the DeliveryManager class
    /// </summary>
    public interface IDeliveryManager
    {
        /// <summary>
        /// Get a delivery
        /// </summary>
        /// <param name="deliveryId">Id of the delivery to get</param>
        /// <returns>Found delivery</returns>
        DeliveryDTO GetDelivery(int deliveryId);

        /// <summary>
        /// Save a new Delivery and update <see cref="dto"/> with the Id assigned to it
        /// </summary>
        /// <param name="dto">Delivery data</param>
        /// <exception cref="NotExistingVehicleException">There is no vehicle with the vehicleId</exception>
        void AddDelivery(DeliveryDTO dto);

        /// <summary>
        /// Assign a Delivery to a Vehicle
        /// </summary>
        /// <param name="deliveryId">Id of the delivery</param>
        /// <param name="vehicleId">Id of the vehicle</param>
        /// <exception cref="NotExistingVehicleException">There is no vehicle with the vehicleId</exception>
        /// <exception cref="NotExistingDeliveryException">There is no delivery with the deliveryId</exception>
        void AssignDeliveryToVehicle(int deliveryId, int vehicleId);

        /// <summary>
        /// Mark a delivery as delivered
        /// </summary>
        /// <param name="deliveryId">Id of the delivery</param>
        void DeliverDelivery(int deliveryId);
    }
}