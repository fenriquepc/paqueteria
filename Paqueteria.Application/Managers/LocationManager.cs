﻿using Paqueteria.Application.DTO;
using Paqueteria.Application.Exceptions;
using Paqueteria.Application.Extensions;
using Paqueteria.Application.Resources;
using Paqueteria.Data.Repositories;
using System;

namespace Paqueteria.Application.Managers
{
    /// <summary>
    /// Manager class to perform application and business logic related with locations
    /// </summary>
    public class LocationManager : ILocationManager
    {
        #region Properties

        private readonly ILocationRepository _locationRepository;
        private readonly IVehicleRepository _vehicleRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="locationRepository">Interface of LocationRepository</param>
        public LocationManager(ILocationRepository locationRepository, IVehicleRepository vehicleRepository)
        {
            _locationRepository = locationRepository;
            _vehicleRepository = vehicleRepository;
        }

        #endregion

        #region Manager Methods

        /// <summary>
        /// Save the current location of a Vehicle
        /// </summary>
        /// <param name="dto">Location data</param>
        /// <exception cref="NotExistingVehicleException">There is no any Vehicle with the id</exception>
        public void SaveCurrentVehicleLocation(LocationDTO dto)
        {
            ValidateVehicle(dto.VehicleId);

            var location = dto.ToEntity();
            location.Date = DateTime.Now;

            _locationRepository.Insert(location);
        }

        #endregion

        #region Private Methods

        private void ValidateVehicle(int vehicleId)
        {
            var vehicle = _vehicleRepository.GetById(vehicleId);
            if (vehicle == null)
            {
                var message = string.Format(Messages.NotExistingVehicleIdTemplate, vehicleId);
                throw new NotExistingVehicleException(message);
            }
        }

        #endregion
    }
}
