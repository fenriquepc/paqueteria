﻿using Paqueteria.Application.DTO;
using Paqueteria.Application.Exceptions;
using Paqueteria.Application.Extensions;
using Paqueteria.Application.Resources;
using Paqueteria.Data.Models;
using Paqueteria.Data.Repositories;
using System;

namespace Paqueteria.Application.Managers
{
    /// <summary>
    /// Manager class to perform application and business logic related with deliveries
    /// </summary>
    public class DeliveryManager : IDeliveryManager
    {
        #region Properties

        private readonly IDeliveryRepository _deliveryRepository;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IVehicleManager _vehicleManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="deliveryRepository">Interface of DeliveryRepository</param>
        /// <param name="vehicleRepository">Interface of VehicleRepository</param>
        public DeliveryManager(IDeliveryRepository deliveryRepository, IVehicleRepository vehicleRepository, IVehicleManager vehicleManager)
        {
            _deliveryRepository = deliveryRepository;
            _vehicleRepository = vehicleRepository;
            _vehicleManager = vehicleManager;
        }

        #endregion

        #region Manager Methods

        /// <summary>
        /// Get a delivery
        /// </summary>
        /// <param name="deliveryId">Id of the delivery to get</param>
        /// <returns>Found delivery</returns>
        public DeliveryDTO GetDelivery(int deliveryId)
        {
            var delivery = _deliveryRepository.GetById(deliveryId);
            if (delivery == null)
            {
                var message = string.Format(Messages.NotExistingDeliveryIdTemplate, deliveryId);
                throw new NotExistingDeliveryException(message);
            }

            var dto = delivery.ToDTO();

            var vehicleId = delivery.VehicleId;
            if (vehicleId.HasValue)
            {
                var currentLocation = _vehicleManager.GetCurrentLocation(vehicleId.Value);
                dto.Location = currentLocation;

                var vehicleDescription = _vehicleManager.GetVehicleDescription(vehicleId.Value);
                dto.VehicleDescription = vehicleDescription;
            }

            return dto;
        }

        /// <summary>
        /// Save a new Delivery and update <see cref="dto"/> with the Id assigned to it
        /// </summary>
        /// <param name="dto">Delivery data</param>
        /// <exception cref="NotExistingVehicleException">There is no vehicle with the vehicleId</exception>
        public void AddDelivery(DeliveryDTO dto)
        {
            if (dto.VehicleId.HasValue)
            {
                ValidateVehicle(dto.VehicleId.Value);
            }

            var delivery = dto.ToEntity();
            _deliveryRepository.Insert(delivery);

            dto.Id = delivery.Id;
        }

        /// <summary>
        /// Assign a Delivery to a Vehicle
        /// </summary>
        /// <param name="deliveryId">Id of the delivery</param>
        /// <param name="vehicleId">Id of the vehicle</param>
        /// <exception cref="NotExistingVehicleException">There is no vehicle with the vehicleId</exception>
        /// <exception cref="NotExistingDeliveryException">There is no delivery with the deliveryId</exception>
        public void AssignDeliveryToVehicle(int deliveryId, int vehicleId)
        {
            ValidateDeliveryAssignmentData(deliveryId, vehicleId, out var delivery);

            delivery.VehicleId = vehicleId;
            _deliveryRepository.Update(delivery);
        }

        /// <summary>
        /// Mark a delivery as delivered
        /// </summary>
        /// <param name="deliveryId">Id of the delivery</param>
        public void DeliverDelivery(int deliveryId)
        {
            ValidateDelivery(deliveryId, out var delivery);

            delivery.Delivered = true;
            delivery.DeliveredDate = DateTime.Now;
            delivery.VehicleId = null;
            _deliveryRepository.Update(delivery);
        }

        #endregion

        #region Private Methods

        private void ValidateDeliveryAssignmentData(int deliveryId, int vehicleId, out Delivery delivery)
        {
            ValidateDelivery(deliveryId, out delivery);
            ValidateVehicle(vehicleId);
        }

        private void ValidateVehicle(int vehicleId)
        {
            var vehicle = _vehicleRepository.GetById(vehicleId);
            if (vehicle == null)
            {
                var message = string.Format(Messages.NotExistingVehicleIdTemplate, vehicleId);
                throw new NotExistingVehicleException(message);
            }
        }

        private void ValidateDelivery(int deliveryId, out Delivery delivery)
        {
            delivery = _deliveryRepository.GetById(deliveryId);
            if (delivery == null)
            {
                var message = string.Format(Messages.NotExistingDeliveryIdTemplate, deliveryId);
                throw new NotExistingDeliveryException(message);
            }

            if (delivery.Delivered)
            {
                var message = Messages.DeliveryAlreadyDeliveredMessage;
                throw new DeliveryAlreadyDeliveredException(message);
            }
        }

        #endregion
    }
}
