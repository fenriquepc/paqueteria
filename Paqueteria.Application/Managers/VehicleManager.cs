﻿using Paqueteria.Application.DTO;
using Paqueteria.Application.Exceptions;
using Paqueteria.Application.Extensions;
using Paqueteria.Application.Resources;
using Paqueteria.Data.Models;
using Paqueteria.Data.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Paqueteria.Application.Managers
{
    /// <summary>
    /// Manager class to perform application and business logic related with vehicles
    /// </summary>
    public class VehicleManager : IVehicleManager
    {
        #region Properties

        private readonly IVehicleRepository _vehicleRepository;
        private readonly ILocationRepository _locationRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="vehicleRepository">Interface of VehicleRepository</param>
        /// <param name="locationRepository">Interface of LocationRepository</param>
        public VehicleManager(IVehicleRepository vehicleRepository, ILocationRepository locationRepository)
        {
            _vehicleRepository = vehicleRepository;
            _locationRepository = locationRepository;
        }

        #endregion

        #region Manager Methods

        /// <summary>
        /// Get all stored vehicles
        /// </summary>
        /// <returns>Stored vehicles</returns>
        public IEnumerable<VehicleDTO> GetVehicles()
        {
            var result = new List<VehicleDTO>();
            foreach (var vehicle in _vehicleRepository.GetAll())
            {
                var locations = GetVehicleLocations(vehicle.Id);
                var dto = vehicle.ToDTO(locations);
                result.Add(dto);
            }

            return result;
        }

        /// <summary>
        /// Get a vehicle
        /// </summary>
        /// <param name="vehicleId">Id of the vehicle to get</param>
        /// <returns>DTO of the Vehicle found</returns>
        /// <exception cref="NotExistingVehicleException">There is no vehicle with the id</exception>
        public VehicleDTO GetVehicle(int vehicleId)
        {
            var vehicle = _vehicleRepository.GetById(vehicleId);
            if (vehicle == null)
            {
                var message = string.Format(Messages.NotExistingVehicleIdTemplate, vehicleId);
                throw new NotExistingVehicleException(message);
            }

            var locations = GetVehicleLocations(vehicleId);
            var dto = vehicle.ToDTO(locations);

            return dto;
        }

        /// <summary>
        /// Get the current location of a vehicle
        /// </summary>
        /// <param name="vehicleId">Id of the vehicle</param>
        /// <returns>Current location of the vehicle</returns>
        public LocationDTO GetCurrentLocation(int vehicleId)
        {
            ValidateVehicle(vehicleId);

            var currentLocation = GetVehicleLocations(vehicleId).FirstOrDefault();
            return currentLocation?.ToDTO();
        }

        /// <summary>
        /// Get a description of a vehicle
        /// </summary>
        /// <param name="vehicleId">Id of the vehicle</param>
        /// <returns>String with a description of the vehicle</returns>
        public string GetVehicleDescription(int vehicleId)
        {
            ValidateVehicle(vehicleId);

            var vehicle = _vehicleRepository.GetById(vehicleId);
            var description = $"{vehicle.Type.ToString()} {vehicle.Model}";

            return description;
        }

        #endregion

        #region Private Methods

        private IEnumerable<Location> GetVehicleLocations(int vehicleId)
        {
            var locations = _locationRepository.GetByVehicle(vehicleId).OrderByDescending(l => l.Date);
            return locations;
        }

        private void ValidateVehicle(int vehicleId)
        {
            var vehicle = _vehicleRepository.GetById(vehicleId);
            if (vehicle == null)
            {
                var message = string.Format(Messages.NotExistingVehicleIdTemplate, vehicleId);
                throw new NotExistingVehicleException(message);
            }
        }

        #endregion
    }
}
