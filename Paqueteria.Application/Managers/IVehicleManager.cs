﻿using Paqueteria.Application.DTO;
using Paqueteria.Data.Models;
using System.Collections.Generic;

namespace Paqueteria.Application.Managers
{
    public interface IVehicleManager
    {
        /// <summary>
        /// Get all stored vehicles
        /// </summary>
        /// <returns>Stored vehicles</returns>
        IEnumerable<VehicleDTO> GetVehicles();

        /// <summary>
        /// Get a vehicle
        /// </summary>
        /// <param name="vehicleId">Id of the vehicle to get</param>
        /// <returns>DTO of the Vehicle found</returns>
        /// <exception cref="NotExistingVehicleException">There is no vehicle with the id</exception>
        VehicleDTO GetVehicle(int vehicleId);

        /// <summary>
        /// Get the current location of a vehicle
        /// </summary>
        /// <param name="vehicleId">Id of the vehicle</param>
        /// <returns>Current location of the vehicle</returns>
        LocationDTO GetCurrentLocation(int vehicleId);

        /// <summary>
        /// Get a description of a vehicle
        /// </summary>
        /// <param name="vehicleId">Id of the vehicle</param>
        /// <returns>String with a description of the vehicle</returns>
        string GetVehicleDescription(int vehicleId);
    }
}