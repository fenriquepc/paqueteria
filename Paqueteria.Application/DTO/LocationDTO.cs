﻿using System;

namespace Paqueteria.Application.DTO
{
    public class LocationDTO
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime Date { get; set; }
    }
}
