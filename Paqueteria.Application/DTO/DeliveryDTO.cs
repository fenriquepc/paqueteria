﻿using System;

namespace Paqueteria.Application.DTO
{
    public class DeliveryDTO
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public int? VehicleId { get; set; }
        public string VehicleDescription { get; set; }
        public bool Delivered { get; set; }
        public DateTime? DeliveredDate { get; set; }
        public LocationDTO Location { get; set; }
    }
}
