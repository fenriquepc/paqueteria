﻿using System.Collections.Generic;

namespace Paqueteria.Application.DTO
{
    public class VehicleDTO
    {
        public int Id { get; set; }
        public string VehicleRegistration { get; set; }
        public string Model { get; set; }
        public string Type { get; set; }
        public IEnumerable<LocationDTO> Locations { get; set; }
    }
}
