﻿using Paqueteria.Application.DTO;
using Paqueteria.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Paqueteria.Application.Extensions
{
    public static class ModelConversions
    {
        public static Location ToEntity(this LocationDTO dto)
        {
            return new Location
            {
                VehicleId = dto.VehicleId,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                Date = dto.Date
            };
        }

        public static LocationDTO ToDTO(this Location location)
        {
            return new LocationDTO
            {
                Id = location.Id,
                VehicleId = location.VehicleId,
                Latitude = location.Latitude,
                Longitude = location.Longitude,
                Date = location.Date
            };
        }

        public static VehicleDTO ToDTO(this Vehicle vehicle, IEnumerable<Location> locations)
        {
            var locationsDTO = locations.Select(l => l.ToDTO()).ToList();
            return new VehicleDTO
            {
                Id = vehicle.Id,
                Model = vehicle.Model,
                Type = vehicle.Type.ToString(),
                VehicleRegistration = vehicle.VehicleRegistration,
                Locations = locationsDTO
            };
        }

        public static Delivery ToEntity(this DeliveryDTO dto)
        {
            return new Delivery
            {
                Id = dto.Id,
                Address = dto.Address,
                VehicleId = dto.VehicleId
            };
        }

        public static DeliveryDTO ToDTO(this Delivery delivery)
        {
            return new DeliveryDTO
            {
                Id = delivery.Id,
                Address = delivery.Address,
                VehicleId = delivery.VehicleId,
                Delivered = delivery.Delivered,
                DeliveredDate = delivery.DeliveredDate
            };
        }
    }
}
