﻿using Newtonsoft.Json;

namespace Paqueteria.API.Models
{
    public class DeliveryPatchRequest
    {
        [JsonProperty("vehicleId")]
        public int VehicleId { get; set; }

        [JsonProperty("delivered")]
        public bool Delivered { get; set; } 
    }
}