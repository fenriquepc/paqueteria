﻿using Newtonsoft.Json;

namespace Paqueteria.API.Models
{
    public class DeliveryPostRequest
    {
        [JsonProperty("address")]
        public string Address { get; set; }
    }
}