﻿using Newtonsoft.Json;
using System;

namespace Paqueteria.API.Models
{
    public class DeliveryGetResponse
    {
        [JsonProperty("address")]
        public string Adress { get; set; }

        [JsonProperty("location")]
        public LocationGetResponse Location { get; set; }

        [JsonProperty("vehicleDescription")]
        public string VehicleDescription { get; set; }

        [JsonProperty("delivered")]
        public bool Delivered { get; set; }

        [JsonProperty("deliveryDate")]
        public DateTime? DeliveryDate { get; set; }
    }
}