﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Paqueteria.API.Models
{
    public class VehicleGetResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("vehicleRegistration")]
        public string VehicleRegistration { get; set; }
        
        [JsonProperty("model")]
        public string Model { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("locations")]
        public IEnumerable<LocationGetResponse> Locations { get; set; }
    }
}