﻿using Newtonsoft.Json;

namespace Paqueteria.API.Models
{
    public class LocationPostRequest
    {
        [JsonProperty("vehicleId")]
        public int VehicleId { get; set; }
        
        [JsonProperty("latitude")]
        public double Latitude { get; set; }
        
        [JsonProperty("longitude")]
        public double Longitude { get; set; }
    }
}