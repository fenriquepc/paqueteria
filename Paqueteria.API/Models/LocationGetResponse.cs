﻿using Newtonsoft.Json;
using System;

namespace Paqueteria.API.Models
{
    public class LocationGetResponse
    {
        [JsonProperty("latitude")]
        public double Latitude { get; set; }
        
        [JsonProperty("longitude")]
        public double Longitude { get; set; }
        
        [JsonProperty("date")]
        public DateTime Date { get; set; }
    }
}