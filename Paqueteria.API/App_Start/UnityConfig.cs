using NLog;
using Paqueteria.Application.Managers;
using Paqueteria.Data.Repositories;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace Paqueteria.API
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);

            #region Managers

            container.RegisterType<IVehicleManager, VehicleManager>();
            container.RegisterType<ILocationManager, LocationManager>();
            container.RegisterType<IDeliveryManager, DeliveryManager>();

            #endregion

            #region Repositories

            container.RegisterType<IVehicleRepository, VehicleRepository>();
            container.RegisterType<ILocationRepository, LocationRepository>();
            container.RegisterType<IDeliveryRepository, DeliveryRepository>();

            #endregion

            #region Others

            container.RegisterFactory<ILogger>(l => LogManager.GetCurrentClassLogger());

            #endregion
        }
    }
}