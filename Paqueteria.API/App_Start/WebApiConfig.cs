﻿using Paqueteria.API.Filters;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace Paqueteria.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web

            // Rutas de API web
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Filters
            config.Filters.Add(new ApplicationExceptionFilterAttribute());

            config.InitializeCustomWebHooks();
            config.InitializeCustomWebHooksApis();

            var traceWriter = config.EnableSystemDiagnosticsTracing();
            traceWriter.IsVerbose = true;
            traceWriter.MinimumLevel = TraceLevel.Error;
        }
    }
}
