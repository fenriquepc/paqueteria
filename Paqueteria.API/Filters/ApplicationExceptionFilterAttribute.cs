﻿using Paqueteria.API.Resources;
using Paqueteria.Application.Exceptions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Paqueteria.API.Filters
{
    public class ApplicationExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public override void OnException(HttpActionExecutedContext context)
        {
            HttpResponseMessage response;
            var exception = context.Exception;
            _logger.Error(exception);

            if (exception is HttpResponseException)
            {
                return;
            }

            if (exception is NotExistingVehicleException)
            {
                response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
            }
            else if (exception is NotExistingDeliveryException)
            {
                response = context.Request.CreateErrorResponse(HttpStatusCode.NotFound, exception.Message);
            }
            else if (exception is DeliveryAlreadyDeliveredException)
            {
                response = context.Request.CreateErrorResponse(HttpStatusCode.Conflict, exception.Message);
            }
            else
            {
                response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, Messages.GenericErrorMessage);
            }

            context.Response = response;
        }
    }
}