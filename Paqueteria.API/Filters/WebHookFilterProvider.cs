﻿using Microsoft.AspNet.WebHooks;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Paqueteria.API.Filters
{
    public class WebHookFilterProvider : IWebHookFilterProvider
    {
        public const string NewLocationEvent = "NewLocation";

        private readonly Collection<WebHookFilter> _filters = new Collection<WebHookFilter>
        {
            new WebHookFilter { Name = NewLocationEvent, Description = "Recibida nueva ubicación"}
        };

        public Task<Collection<WebHookFilter>> GetFiltersAsync()
        {
            return Task.FromResult(_filters);
        }
    }
}