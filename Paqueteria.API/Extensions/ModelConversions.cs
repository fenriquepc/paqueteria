﻿using Paqueteria.API.Models;
using Paqueteria.Application.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Paqueteria.API.Extensions
{
    public static class ModelConversions
    {
        public static LocationDTO ToDTO(this LocationPostRequest locationPostRequest)
        {
            return new LocationDTO
            {
                VehicleId = locationPostRequest.VehicleId,
                Latitude = locationPostRequest.Latitude,
                Longitude = locationPostRequest.Longitude
            };
        }

        public static LocationGetResponse ToResponse(this LocationDTO dto)
        {
            return new LocationGetResponse
            {
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                Date = dto.Date
            };
        }

        public static VehicleGetResponse ToResponse(this VehicleDTO dto)
        {
            var locations = dto.Locations.Select(l => l.ToResponse()).ToList();
            return new VehicleGetResponse
            {
                Id = dto.Id,
                Model = dto.Model,
                Type = dto.Type,
                VehicleRegistration = dto.VehicleRegistration,
                Locations = locations
            };
        }

        public static IEnumerable<VehicleGetResponse> ToResponse(this IEnumerable<VehicleDTO> dtos)
        {
            var result = new List<VehicleGetResponse>();
            foreach (var dto in dtos)
            {
                var response = dto.ToResponse();
                result.Add(response);
            }

            return result;
        }

        public static DeliveryDTO ToDTO(this DeliveryPostRequest deliveryPostRequest)
        {
            return new DeliveryDTO
            {
                Address = deliveryPostRequest.Address
            };
        }

        public static DeliveryGetResponse ToResponse(this DeliveryDTO dto)
        {
            var location = dto.Location?.ToResponse();
            return new DeliveryGetResponse
            {
                Adress = dto.Address,
                Location = location,
                VehicleDescription = dto.VehicleDescription,
                Delivered = dto.Delivered,
                DeliveryDate = dto.DeliveredDate
            };
        }
    }
}