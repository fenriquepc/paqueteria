﻿using Newtonsoft.Json;
using NLog;
using Paqueteria.API.Extensions;
using Paqueteria.API.Filters;
using Paqueteria.API.Models;
using Paqueteria.API.Resources;
using Paqueteria.Application.Managers;
using System.Web.Http;

namespace Paqueteria.API.Controllers
{
    public class LocationController : BaseController
    {
        #region Properties

        private readonly ILocationManager _manager;

        #endregion

        #region Constructors

        public LocationController(ILocationManager locationManager, ILogger logger) : base(logger)
        {
            _manager = locationManager;
        }

        #endregion

        #region Controller Methods

        // POST api/<controller>
        public async void Post(LocationPostRequest data)
        {
            var traceMessage = string.Format(Messages.PostRequestReceivedTemplate, JsonConvert.SerializeObject(data));
            _logger.Trace(traceMessage);

            var dto = data.ToDTO();
            _manager.SaveCurrentVehicleLocation(dto);

            await this.NotifyAllAsync(WebHookFilterProvider.NewLocationEvent, new { dto.VehicleId });
        }

        #endregion
    }
}