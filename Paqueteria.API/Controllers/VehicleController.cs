﻿using NLog;
using Paqueteria.API.Extensions;
using Paqueteria.API.Models;
using Paqueteria.API.Resources;
using Paqueteria.Application.Managers;
using System.Collections.Generic;

namespace Paqueteria.API.Controllers
{
    public class VehicleController : BaseController
    {
        #region Properties

        private readonly IVehicleManager _manager;

        #endregion

        #region Constructors

        public VehicleController(IVehicleManager manager, ILogger logger) : base(logger)
        {
            _manager = manager;
        }

        #endregion

        #region Controller Methods

        // GET api/<controller>
        public IEnumerable<VehicleGetResponse> Get()
        {
            _logger.Trace(string.Format(Messages.GetRequestNoParametersReceivedMessage));

            var dtos = _manager.GetVehicles();
            return dtos.ToResponse();
        }

        // GET api/<controller>/5
        public VehicleGetResponse Get(int id)
        {
            var traceMessage = string.Format(Messages.GetRequestIdReceivedTemplate, id);
            _logger.Trace(traceMessage);

            var dto = _manager.GetVehicle(id);
            var response = dto.ToResponse();
            
            return response;
        }

        #endregion
    }
}