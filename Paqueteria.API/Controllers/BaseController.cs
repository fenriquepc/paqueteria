﻿using NLog;
using System.Web.Http;

namespace Paqueteria.API.Controllers
{
    public class BaseController : ApiController
    {
        #region Properties

        protected readonly ILogger _logger;

        #endregion

        #region Constructors

        public BaseController(ILogger logger)
        {
            _logger = logger;
        }

        #endregion
    }
}