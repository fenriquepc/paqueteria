﻿using Newtonsoft.Json;
using NLog;
using Paqueteria.API.Extensions;
using Paqueteria.API.Models;
using Paqueteria.API.Resources;
using Paqueteria.Application.Managers;
using System.Web.Http;

namespace Paqueteria.API.Controllers
{
    public class DeliveryController : BaseController
    {
        #region Properties

        private readonly IDeliveryManager _manager;

        #endregion

        #region Constructors

        public DeliveryController(IDeliveryManager manager, ILogger logger) : base(logger)
        {
            _manager = manager;
        }

        #endregion

        // GET api/<controller>/5
        public DeliveryGetResponse Get(int id)
        {
            var traceMessage = string.Format(Messages.GetRequestIdReceivedTemplate, id);
            _logger.Trace(traceMessage);

            var dto = _manager.GetDelivery(id);
            return dto.ToResponse();
        }

        // POST api/<controller>
        public IHttpActionResult Post(DeliveryPostRequest data)
        {
            var traceMessage = string.Format(Messages.PostRequestReceivedTemplate, JsonConvert.SerializeObject(data));
            _logger.Trace(traceMessage);

            var dto = data.ToDTO();
            _manager.AddDelivery(dto);

            return CreatedAtRoute("DefaultApi", new { id = dto.Id }, dto.ToResponse());
        }

        // PATCH api/<controller>/5
        public void Patch(int id, DeliveryPatchRequest data)
        {
            var traceMessage = string.Format(Messages.PatchRequestReceivedTemplate, id, JsonConvert.SerializeObject(data));
            _logger.Trace(traceMessage);

            var delivered = data.Delivered;
            if (delivered)
            {
                _manager.DeliverDelivery(id);
            } 
            else
            {
                var vehicleId = data.VehicleId;
                _manager.AssignDeliveryToVehicle(id, vehicleId);
            }
        }
    }
}