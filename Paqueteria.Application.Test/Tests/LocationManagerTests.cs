﻿using NUnit.Framework;
using Paqueteria.Application.DTO;
using Paqueteria.Application.Exceptions;
using Paqueteria.Application.Managers;
using Paqueteria.Application.Test.Mocks;
using Paqueteria.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Paqueteria.Application.Test.Tests
{
    public class LocationManagerTests
    {
        #region Properties

        ILocationManager _manager;
        LocationRepositoryMock _locationRepositoryMock;
        VehicleRepositoryMock _vehicleRepositoryMock;

        private const int VEHICLE_ID = 1;
        private const int VEHICLE_ID_NOT_EXISTS = 2;

        private IEnumerable<Vehicle> _vehicleRepositoryData;

        private LocationDTO locationDTOOk;
        private LocationDTO locationDTOVehicleNotExists;
        private Vehicle existingVehicle;

        #endregion

        #region Setup Metods

        [SetUp]
        public void Setup()
        {
            DataSetup();
            SetupLocationRepositoryMock();
            SetupVehicleRepositoryMock();
            _manager = new LocationManager(_locationRepositoryMock.Object, _vehicleRepositoryMock.Object);
        }

        private void DataSetup()
        {
            locationDTOOk = new LocationDTO
            {
                Latitude = 1.8,
                Longitude = 2.2,
                VehicleId = VEHICLE_ID
            };

            locationDTOVehicleNotExists = new LocationDTO
            {
                Latitude = 1.6,
                Longitude = 2.1,
                VehicleId = VEHICLE_ID_NOT_EXISTS
            };

            existingVehicle = new Vehicle
            {
                Id = VEHICLE_ID
            };


            _vehicleRepositoryData = new List<Vehicle>()
            {
                existingVehicle
            };
        }

        private void SetupLocationRepositoryMock()
        {
            _locationRepositoryMock = new LocationRepositoryMock();
        }

        private void SetupVehicleRepositoryMock()
        {
            _vehicleRepositoryMock = new VehicleRepositoryMock();
            _vehicleRepositoryMock.AddData(_vehicleRepositoryData);
        }

        #endregion

        #region SaveCurrentVehicleLocation Tests

        [Test]
        public void SaveCurrentVehicleLocationOk()
        {
            _manager.SaveCurrentVehicleLocation(locationDTOOk);

            var locationAdded = _locationRepositoryMock.Data.Last();
            Assert.AreEqual(1, locationAdded.Id);
            Assert.AreEqual(VEHICLE_ID, locationAdded.VehicleId);
            Assert.AreNotEqual(new DateTime(), locationAdded.Date);
        }

        [Test]
        public void SaveCurrentVehicleLocationVehicleNotExists()
        {
            Assert.Throws<NotExistingVehicleException>(() => _manager.SaveCurrentVehicleLocation(locationDTOVehicleNotExists));
        }

        #endregion
    }
}
