﻿using NUnit.Framework;
using Paqueteria.Application.Exceptions;
using Paqueteria.Application.Managers;
using Paqueteria.Application.Test.Mocks;
using Paqueteria.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Paqueteria.Application.Test.Tests
{
    public class VehicleManagerTests
    {
        #region Properties

        private IVehicleManager _manager;
        private VehicleRepositoryMock _vehicleRepositoryMock;
        private LocationRepositoryMock _locationRepositoryMock;

        private const int VEHICLE_ID_OK = 1;
        private const VehicleTypeEnum VEHICLE_TYPE = VehicleTypeEnum.Truck;
        private const string VEHICLE_MODEL = "MAN TGX";
        private const string VEHICLE_REGISTRATION = "0453CMU";
        private const int VEHICLE2_ID = 2;
        private const VehicleTypeEnum VEHICLE2_TYPE = VehicleTypeEnum.Van;
        private const string VEHICLE2_MODEL = "Mercedes Vito";
        private const string VEHICLE2_REGISTRATION = "8204CKJ";
        private const int VEHICLE_ID_NOT_EXISTS = 3;
        private const int VEHICLE_ID_WITHOUT_LOCATIONS = 4;
        private const int LOCATION1_ID = 1;
        private const double LOCATION1_LATITUDE = 1;
        private const double LOCATION1_LONGITUDE = 1;
        private static readonly DateTime LOCATION1_DATE = new DateTime(2019, 12, 2);
        private const int LOCATION2_ID = 2;
        private const double LOCATION2_LATITUDE = 2.1;
        private const double LOCATION2_LONGITUDE = 2.5;
        private static readonly DateTime LOCATION2_DATE = new DateTime(2019, 12, 12);
        private const int LOCATION3_ID = 2;
        private const double LOCATION3_LATITUDE = 2.9;
        private const double LOCATION3_LONGITUDE = 2.0;
        private static readonly DateTime LOCATION3_DATE = new DateTime(2019, 12, 20);

        private IEnumerable<Vehicle> _vehicleRepositoryData;
        private IEnumerable<Location> _locationRepositoryData;

        private Vehicle existingVehicle;
        private Vehicle existingVehicleWithoutLocations;
        private Location location1;
        private Location location2;
        private Location location3;

        #endregion

        #region Setup Methods

        [SetUp]
        public void Setup()
        {
            DataSetup();
            SetupVehicleRepositoryMock();
            SetupLocationRepositoryMock();
            _manager = new VehicleManager(_vehicleRepositoryMock.Object, _locationRepositoryMock.Object);
        }

        private void DataSetup()
        {
            existingVehicle = new Vehicle
            {
                Id = VEHICLE_ID_OK,
                VehicleRegistration = VEHICLE_REGISTRATION,
                Model = VEHICLE_MODEL,
                Type = VEHICLE_TYPE
            };

            existingVehicleWithoutLocations = new Vehicle
            {
                Id = VEHICLE_ID_WITHOUT_LOCATIONS,
                VehicleRegistration = VEHICLE2_REGISTRATION,
                Model = VEHICLE2_MODEL,
                Type = VEHICLE2_TYPE
            };

            location1 = new Location
            {
                Id = LOCATION1_ID,
                Latitude = LOCATION1_LATITUDE,
                Longitude = LOCATION1_LONGITUDE,
                Date = LOCATION1_DATE,
                VehicleId = VEHICLE_ID_OK
            };

            location2 = new Location
            {
                Id = LOCATION2_ID,
                Latitude = LOCATION2_LATITUDE,
                Longitude = LOCATION2_LONGITUDE,
                Date = LOCATION2_DATE,
                VehicleId = VEHICLE_ID_OK
            };

            location3 = new Location
            {
                Id = LOCATION3_ID,
                Latitude = LOCATION3_LATITUDE,
                Longitude = LOCATION3_LONGITUDE,
                Date = LOCATION3_DATE,
                VehicleId = VEHICLE2_ID
            };

            _vehicleRepositoryData = new List<Vehicle>()
            {
                existingVehicle,
                existingVehicleWithoutLocations
            };

            _locationRepositoryData = new List<Location>()
            {
                location1,
                location2,
                location3
            };
        }

        private void SetupVehicleRepositoryMock()
        {
            _vehicleRepositoryMock = new VehicleRepositoryMock();
            _vehicleRepositoryMock.AddData(_vehicleRepositoryData);
        }

        private void SetupLocationRepositoryMock()
        {
            _locationRepositoryMock = new LocationRepositoryMock();
            _locationRepositoryMock.AddData(_locationRepositoryData);
        }

        #endregion

        #region GetVehicles Tests

        [Test]
        public void GetVehiclesOk()
        {
            var vehicles = _manager.GetVehicles();

            Assert.AreEqual(_vehicleRepositoryData.Count(), vehicles.Count());
            foreach (var vehicleDTO in vehicles)
            {
                var vehicle = _vehicleRepositoryData.First(r => r.Id == vehicleDTO.Id);
                Assert.AreEqual(vehicle.Id, vehicleDTO.Id);
                Assert.AreEqual(vehicle.Model, vehicleDTO.Model);
                Assert.AreEqual(vehicle.Type.ToString(), vehicleDTO.Type);
                Assert.AreEqual(vehicle.VehicleRegistration, vehicleDTO.VehicleRegistration);
            }
        }

        #endregion

        #region GetVehicle Tests

        [Test]
        public void GetVehicleOk()
        {
            var vehicleDTO = _manager.GetVehicle(VEHICLE_ID_OK);

            Assert.AreEqual(VEHICLE_ID_OK, vehicleDTO.Id);
            Assert.AreEqual(existingVehicle.Model, vehicleDTO.Model);
            Assert.AreEqual(existingVehicle.Type.ToString(), vehicleDTO.Type);
            Assert.AreEqual(existingVehicle.VehicleRegistration, vehicleDTO.VehicleRegistration);
        }

        [Test]
        public void GetVehicleNotExists()
        {
            Assert.Throws<NotExistingVehicleException>(() => _manager.GetVehicle(VEHICLE_ID_NOT_EXISTS));
        }

        #endregion

        #region GetCurrentLocation Tests

        [Test]
        public void GetCurrentLocationOk()
        {
            var location = _manager.GetCurrentLocation(VEHICLE_ID_OK);

            var lastLocation = _locationRepositoryData.OrderByDescending(l => l.Date).First(l => l.VehicleId == VEHICLE_ID_OK);
            Assert.AreEqual(lastLocation.Date, location.Date);
            Assert.AreEqual(lastLocation.Id, location.Id);
            Assert.AreEqual(lastLocation.Latitude, location.Latitude);
            Assert.AreEqual(lastLocation.Longitude, location.Longitude);
        }

        [Test]
        public void GetCurrentLocationVehicleNotExists()
        {
            Assert.Throws<NotExistingVehicleException>(() => _manager.GetCurrentLocation(VEHICLE_ID_NOT_EXISTS));
        }

        [Test]
        public void GetCurrentLocationVehicleWithoutLocations()
        {
            var location = _manager.GetCurrentLocation(VEHICLE_ID_WITHOUT_LOCATIONS);
            Assert.IsNull(location);
        }

        #endregion

        #region GetVehicleDescription Tests

        [Test]
        public void GetVehicleDescriptionOk()
        {
            var result = _manager.GetVehicleDescription(VEHICLE_ID_OK);
            var expected = $"{VEHICLE_TYPE.ToString()} {VEHICLE_MODEL}";
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetVehicleDescriptionVehicleNotExists()
        {
            Assert.Throws<NotExistingVehicleException>(() => _manager.GetVehicleDescription(VEHICLE_ID_NOT_EXISTS));
        }

        #endregion
    }
}
