﻿using Moq;
using NUnit.Framework;
using Paqueteria.Application.DTO;
using Paqueteria.Application.Exceptions;
using Paqueteria.Application.Managers;
using Paqueteria.Application.Test.Mocks;
using Paqueteria.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Paqueteria.Application.Test.Tests
{
    public class DeliveryManagerTests
    {
        #region Properties

        IDeliveryManager _manager;
        DeliveryRepositoryMock _deliveryRepositoryMock;
        VehicleRepositoryMock _vehicleRepositoryMock;
        VehicleManagerManagerMock _vehicleManagerMock;

        private const int DELIVERY_ID = 1;
        private const int VEHICLE_ID_OK = 1;
        private const VehicleTypeEnum VEHICLE_TYPE = VehicleTypeEnum.Truck;
        private const string VEHICLE_MODEL = "MAN TGX";
        private const string VEHICLE_REGISTRATION = "0453CMU";
        private const string VEHICLE_DESCRIPTION = "Truck MAN";
        private const string ADDRESS_OK = "calle El Pino 34";
        private const int VEHICLE_ID_NOT_EXISTS = 2;
        private const int DELIVERY_ID_NOT_EXISTS = 4;
        private const string DELIVERY_ADDRESS_EXISTING = "avenida Principal 4";
        private const int DELIVERY2_ID = 2;
        private const string DELIVERY2_ADDRESS = "calle Del Colegio 2";
        private const int DELIVERY3_ID = 3;
        private const string DELIVERY3_ADDRESS = "calle Pio Baroja 5";
        private const int LOCATION_ID = 1;
        private const double LOCATION_LATITUDE = 2.1;
        private const double LOCATION_LONGITUDE = 3.6;
        private readonly static DateTime LOCATION_DATE = new DateTime(2019, 12, 21);

        private IEnumerable<Delivery> _deliveryRepositoryData;
        private IEnumerable<Vehicle> _vehicleRepositoryData;

        private DeliveryDTO dtoOk;
        private DeliveryDTO dtoVehicleNotExists;
        private LocationDTO vehicleLocationDTO;

        private Delivery existingDeliveryWithoutVehicle;
        private Delivery existingDelivery;
        private Delivery existingDeliveryDelivered;
        private Vehicle existingVehicle;

        #endregion

        #region Setup Methods

        [SetUp]
        public void Setup()
        {
            DataSetup();
            SetupDeliveryRepositoryMock();
            SetupVehicleRepositoryMock();
            SetupVehicleManagerMock();
            _manager = new DeliveryManager(_deliveryRepositoryMock.Object, _vehicleRepositoryMock.Object, _vehicleManagerMock.Object);
        }

        private void DataSetup()
        {
            dtoOk = new DeliveryDTO
            {
                VehicleId = VEHICLE_ID_OK,
                Address = ADDRESS_OK
            };

            dtoVehicleNotExists = new DeliveryDTO
            {
                VehicleId = VEHICLE_ID_NOT_EXISTS,
                Address = ADDRESS_OK
            };

            vehicleLocationDTO = new LocationDTO
            {
                Id = LOCATION_ID,
                Latitude = LOCATION_LATITUDE,
                Longitude = LOCATION_LONGITUDE,
                Date = LOCATION_DATE,
                VehicleId = VEHICLE_ID_OK
            };

            existingDeliveryWithoutVehicle = new Delivery
            {
                Id = DELIVERY_ID,
                Address = DELIVERY_ADDRESS_EXISTING
            };

            existingDelivery = new Delivery
            {
                Id = DELIVERY2_ID,
                Address = DELIVERY2_ADDRESS,
                VehicleId = VEHICLE_ID_OK
            };

            existingDeliveryDelivered = new Delivery
            {
                Id = DELIVERY3_ID,
                Address = DELIVERY3_ADDRESS,
                Delivered = true,
                DeliveredDate = DateTime.Now,
            };

            existingVehicle = new Vehicle
            {
                Id = VEHICLE_ID_OK,
                VehicleRegistration = VEHICLE_REGISTRATION,
                Model = VEHICLE_MODEL,
                Type = VEHICLE_TYPE
            };


            _deliveryRepositoryData = new List<Delivery>()
            {
                existingDeliveryWithoutVehicle,
                existingDelivery,
                existingDeliveryDelivered
            };

            _vehicleRepositoryData = new List<Vehicle>()
            {
                existingVehicle
            };
        }

        private void SetupDeliveryRepositoryMock()
        {
            _deliveryRepositoryMock = new DeliveryRepositoryMock();
            _deliveryRepositoryMock.AddData(_deliveryRepositoryData);
        }

        private void SetupVehicleRepositoryMock()
        {
            _vehicleRepositoryMock = new VehicleRepositoryMock();
            _vehicleRepositoryMock.AddData(_vehicleRepositoryData);
        }

        private void SetupVehicleManagerMock()
        {
            _vehicleManagerMock = new VehicleManagerManagerMock();
            _vehicleManagerMock.Setup(
                m => m.GetCurrentLocation(It.Is<int>(i => i == VEHICLE_ID_OK))
            ).Returns(
                vehicleLocationDTO
            );

            _vehicleManagerMock.Setup(
                m => m.GetVehicleDescription(It.IsAny<int>())
            ).Returns(
                VEHICLE_DESCRIPTION
            );
        }

        #endregion

        #region GetDelivery Tests

        [Test]
        public void GetDeliveryOk()
        {
            var delivery = _manager.GetDelivery(DELIVERY2_ID);

            Assert.AreEqual(existingDelivery.Id, delivery.Id);
            Assert.AreEqual(existingDelivery.Address, delivery.Address);
            Assert.AreEqual(existingDelivery.VehicleId, delivery.VehicleId);
            Assert.AreEqual(VEHICLE_DESCRIPTION, delivery.VehicleDescription);

            var deliveryLocation = delivery.Location;
            Assert.AreEqual(LOCATION_ID, deliveryLocation.Id);
            Assert.AreEqual(LOCATION_LATITUDE, deliveryLocation.Latitude);
            Assert.AreEqual(LOCATION_LONGITUDE, deliveryLocation.Longitude);
            Assert.AreEqual(LOCATION_DATE, deliveryLocation.Date);
            Assert.AreEqual(VEHICLE_ID_OK, deliveryLocation.VehicleId);
        }

        [Test]
        public void GetDeliveryWithoutVehicleOk()
        {
            var delivery = _manager.GetDelivery(DELIVERY_ID);

            Assert.AreEqual(existingDeliveryWithoutVehicle.Id, delivery.Id);
            Assert.AreEqual(existingDeliveryWithoutVehicle.Address, delivery.Address);
            Assert.AreEqual(existingDeliveryWithoutVehicle.VehicleId, delivery.VehicleId);
            Assert.IsNull(delivery.Location);
        }

        [Test]
        public void GetDeliveryNotExists()
        {
            Assert.Throws<NotExistingDeliveryException>(() => _manager.GetDelivery(DELIVERY_ID_NOT_EXISTS));
        }

        #endregion

        #region AddDelivery Tests

        [Test]
        public void AddDeliveryOk()
        {
            _manager.AddDelivery(dtoOk);
            var newId = _deliveryRepositoryData.Count() + 1;

            var deliveryAdded = _deliveryRepositoryMock.Data.Last();
            Assert.AreEqual(newId, dtoOk.Id);
            Assert.AreEqual(newId, deliveryAdded.Id);
            Assert.AreEqual(VEHICLE_ID_OK, deliveryAdded.VehicleId);
            Assert.AreEqual(ADDRESS_OK, deliveryAdded.Address);
        }

        [Test]
        public void AddDeliveryVehicleNotExists()
        {
            Assert.Throws<NotExistingVehicleException>(() => _manager.AddDelivery(dtoVehicleNotExists));
        }

        #endregion

        #region AssignDeliveryToVehicle Tests

        [Test]
        public void AssignDeliveryToVehicleOk()
        {
            _manager.AssignDeliveryToVehicle(DELIVERY_ID, VEHICLE_ID_OK);

            var deliveryAffected = _deliveryRepositoryMock.Data.First(d => d.Id == DELIVERY_ID);
            Assert.AreEqual(_deliveryRepositoryData.Count(), _deliveryRepositoryMock.Data.Count());
            Assert.AreEqual(VEHICLE_ID_OK, deliveryAffected.VehicleId);
            Assert.AreEqual(DELIVERY_ADDRESS_EXISTING, deliveryAffected.Address);
        }

        [Test]
        public void AssignDeliveryToVehicleVehicleNotExists()
        {
            Assert.Throws<NotExistingVehicleException>(() => _manager.AssignDeliveryToVehicle(DELIVERY_ID, VEHICLE_ID_NOT_EXISTS));
        }

        [Test]
        public void AssignDeliveryToVehicleDeliveryNotExists()
        {
            Assert.Throws<NotExistingDeliveryException>(() => _manager.AssignDeliveryToVehicle(DELIVERY_ID_NOT_EXISTS, VEHICLE_ID_OK));
        }

        [Test]
        public void AssignDeliveryToVehicleDeliveryDelivered()
        {
            Assert.Throws<DeliveryAlreadyDeliveredException>(() => _manager.AssignDeliveryToVehicle(DELIVERY3_ID, VEHICLE_ID_OK));
        }

        #endregion

        #region DeliverDelivery Tests

        [Test]
        public void DeliverDeliveryOk()
        {
            _manager.DeliverDelivery(DELIVERY_ID);

            var deliveryAffected = _deliveryRepositoryMock.Data.First(d => d.Id == DELIVERY_ID);
            Assert.AreEqual(true, deliveryAffected.Delivered);
            Assert.IsTrue(deliveryAffected.DeliveredDate.HasValue);
            Assert.IsNull(deliveryAffected.VehicleId);
        }

        [Test]
        public void DeliverDeliveryDeliveryNotExists()
        {
            Assert.Throws<NotExistingDeliveryException>(() => _manager.DeliverDelivery(DELIVERY_ID_NOT_EXISTS));
        }

        [Test]
        public void DeliverDeliveryDeliveryDelivered()
        {
            Assert.Throws<DeliveryAlreadyDeliveredException>(() => _manager.DeliverDelivery(DELIVERY3_ID));
        }

        #endregion
    }
}
