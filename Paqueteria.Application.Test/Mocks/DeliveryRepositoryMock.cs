﻿using Moq;
using Paqueteria.Data.Models;
using Paqueteria.Data.Repositories;
using System.Linq;

namespace Paqueteria.Application.Test.Mocks
{
    public class DeliveryRepositoryMock : BaseRepositoryMock<Delivery, IDeliveryRepository>
    {
        public DeliveryRepositoryMock()
        {
            Setup(
                r => r.GetById(It.IsAny<int>())
            ).Returns(
                (int id) => Data.FirstOrDefault(d => d.Id == id)
            );

            Setup(
                r => r.Insert(It.IsAny<Delivery>())
            ).Callback(
                (Delivery entity) => {
                    entity.Id = Data.Count + 1;
                    Data.Add(entity);
                }
            );
        }
    }
}
