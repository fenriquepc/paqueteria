﻿using Moq;
using Paqueteria.Data.Repositories;
using System.Collections.Generic;

namespace Paqueteria.Application.Test.Mocks
{
    public class BaseRepositoryMock<T, U> : Mock<U> where U : class, IRepository<T>
    {
        public readonly List<T> Data = new List<T>();

        public BaseRepositoryMock()
        {
            Setup(
                r => r.Insert(It.IsAny<T>())
            ).Callback(
                (T entity) => Data.Add(entity)
            );

            Setup(
                r => r.GetAll()
            ).Returns(
                Data
            );
        }

        #region Auxiliar Methods

        public void AddData(IEnumerable<T> entities)
        {
            Data.AddRange(entities);
        }

        #endregion
    }
}
