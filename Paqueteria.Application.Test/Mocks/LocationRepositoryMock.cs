﻿using Moq;
using Paqueteria.Data.Models;
using Paqueteria.Data.Repositories;
using System.Linq;

namespace Paqueteria.Application.Test.Mocks
{
    public class LocationRepositoryMock : BaseRepositoryMock<Location, ILocationRepository>
    {
        public LocationRepositoryMock()
        {
            Setup(
                r => r.Insert(It.IsAny<Location>())
            ).Callback(
                (Location entity) => {
                    entity.Id = Data.Count + 1;
                    Data.Add(entity);
                }
            );

            Setup(
                r => r.GetByVehicle(It.IsAny<int>())
            ).Returns(
                (int id) => Data.Where(l => l.VehicleId == id)
            );
        }
    }
}
