﻿using Moq;
using Paqueteria.Data.Models;
using Paqueteria.Data.Repositories;
using System.Linq;

namespace Paqueteria.Application.Test.Mocks
{
    public class VehicleRepositoryMock : BaseRepositoryMock<Vehicle, IVehicleRepository>
    {
        public VehicleRepositoryMock()
        {
            Setup(
                r => r.GetById(It.IsAny<int>())
            ).Returns(
                (int id) => Data.FirstOrDefault(d => d.Id == id)
            );
        }
    }
}
