# Ejemplo de API REST para paquetería

## Escenario
Una empresa de paquetería ha creado una nueva función para sus clientes, que les permite rastrear en tiempo real dónde se encuentra su pedido y en qué vehículo.

Actualmente, ya cuenta con una aplicación móvil y un sitio web, por lo que se necesita crear una nueva API con la que los vehículos puedan conectarse y añadir pedidos a su reparto, y los usuarios puedan obtener la posición actual de su pedido y el vehículo que realizará la entrega indicando el número de pedido.

La solución contiene dos proyectos principales.

## Paqueteria.API
Una API REST que expone las funcionalidades que se requieren.

## LocationUpdatesReceiver
Aplicación de ejemplo que se suscribirá a un webhook de la API que notifica cada vez que se registra una nueva ubicación.

## Instrucciones
Para que la comunicación por webhooks entre la API y la aplicación funcione, el servidor IIS que esté ejecutando la API deberá tener habilitada la autenticación integrada de Windows.

Para ello, en el caso de levantar la API desde Visual Studio en un IIS Express, es necesario modificar el fichero *.vs\Paqueteria\config\applicationhost.config* dentro del directorio de la solución de la siguiente forma:

```xml
<windowsAuthentication enabled="true">
    <providers>
        <add value="Negotiate" />
        <add value="NTLM" />
    </providers>
</windowsAuthentication>
```

En caso de usar un IIS Express externo, el fichero se encuentra en *'<directorio del usuario'>\Documents\IISExpress\config\applicationhost.config*.

Por otro lado, también es necesario indicar en el app.config de LocationUpdaterReceiver la url base donde se levantará la API (*webhookSenderBaseAddress*), así como la url base donde la aplicación levantará el servidor receptor del webhook (*webhookReceiverBaseAddress*):

```xml
<add key="webhookSenderBaseAddress" value="https://localhost:44301"/>
<add key="webhookReceiverBaseAddress" value="http://localhost:9001"/>
```