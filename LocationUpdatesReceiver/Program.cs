﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using LocationUpdatesReceiver.Models;
using Microsoft.Owin.Hosting;

namespace LocationUpdatesReceiver
{
    class Program
    {
        private static readonly string _webhookSenderBaseAddress = ConfigurationManager.AppSettings["webhookSenderBaseAddress"];
        private static readonly string _webhookReceiverBaseAddress = ConfigurationManager.AppSettings["webhookReceiverBaseAddress"];

        static async Task Main(string[] args)
        {
            var handler = new HttpClientHandler
            {
                UseDefaultCredentials = true
            };

            // Start OWIN host 
            using (WebApp.Start<Startup>(_webhookReceiverBaseAddress))
            using (var httpClient = new HttpClient(handler))
            {
                // User should wait until the server (WebApiHost) is up and running before registering
                Console.WriteLine("Webhook receiver running");
                Console.WriteLine("Wait for the server to be ready. Then press any key to register this client on the webhook sender");
                Console.ReadKey();

                // Create a webhook registration to our custom controller
                var registration = new Registration
                {
                    WebHookUri = $"{_webhookReceiverBaseAddress}/api/webhooks/incoming/custom",
                    Description = "New location registered.",
                    Secret = "12345678901234567890123456789012",

                    // Remove the line below to receive all events, including the MessageRemovedEvent event.
                    Filters = new List<string> { "NewLocation" }
                };

                // Register our webhook using the custom controller
                var result = await httpClient.PostAsJsonAsync($"{_webhookSenderBaseAddress}/api/webhooks/registrations", registration);
                
                Console.WriteLine(result.IsSuccessStatusCode ? "Registration succesful" : "Registration failed");
                Console.WriteLine("Press 'q' to quit");

                ConsoleKey key;
                do
                {
                    key = Console.ReadKey().Key;
                } while (key != ConsoleKey.Q);
            }
        }
    }
}
